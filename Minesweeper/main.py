import random

def random_bom(rows: int, cols: int, bom_number: int) -> list:
    # mine = []
    
    # for row in range(rows):
    #     row_data = []
    #     for col in range(cols):
    #         row_data.append(0)
    #     mine.append(row_data)

    # while (bom_number > 0):
    #     row_random = random.randint(0,rows-1)
    #     col_random = random.randint(0,cols-1)

    #     if(mine[row_random][col_random] != -1):
    #         mine[row_random][col_random] = -1
    #         bom_number -= 1

    mine = [[0]*cols for i in range(rows)]

    counter = min(bom_number,rows*cols)

    while (counter > 0):
        row_random = random.randint(0,rows-1)
        col_random = random.randint(0,cols-1)

        if(mine[row_random][col_random] != -1):
            mine[row_random][col_random] = -1
            counter -= 1

    return mine

if __name__ == '__main__':
    mine = random_bom(5,5,5)
    for row in mine:
        print(row)