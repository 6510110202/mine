from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.popup import Popup

import random 

class Field(Button):
    def __init__(self, **kwargs):
        self.row_id = kwargs.pop("row_id")
        self.col_id = kwargs.pop("col_id")
        super().__init__(**kwargs)
    
    def on_press(self):
        print(self.row_id, self.col_id)
        print(self.parent.mine[self.row_id][self.col_id])
        self.disabled = True
        if self.parent.mine[self.row_id][self.col_id] == 0:
            self.text = ''
            self.parent.setting['right_click_count'] += 1
            print(self.parent.parent)
            self.parent.parent.ids['score_number'].text = str(
                self.parent.setting['right_click_count']
            )
            return
        self.text = "bomb"
        self.source = "image/bomb-small.png"

class PlayGameScreen(Screen):
    def __init__(self, **kw):
        self.setting = kw.pop('setting')
        super().__init__(**kw)

    def on_pre_enter(self):
        self.mine_layout_box = self.ids['mine_layout']
        self.add_widget(
            MineLayout(
            rows=self.setting['rows'],
            cols=self.setting['cols'], 
            setting = self.setting))
        

class LoginScreen(Screen):
    def __init__(self, **kw):
        self.setting = kw.pop('setting')
        super().__init__(**kw)

    def select_size(self, rows, cols, bom_number):
        print('select size', rows, cols)
        self.setting['cols'] = cols
        self.setting['rows'] = rows
        self.setting['bom_number'] = bom_number

        self.manager.current = 'game'

class BombPopup(Popup):
    pass

class MineLayout(GridLayout):

    def __init__(self, **kwargs):
        self.setting = kwargs.pop('setting')
        super().__init__(**kwargs)
        self.mine = self.random_bom(
            self.setting['rows'], self.setting['cols'], self.setting['bom_number']
        )
        self.generate_field(self.setting['rows'], self.setting['cols'])
        print(self.mine)
       
    def generate_field(self,rows,cols):
        for i in range(self.setting['rows']):
            for j in range(self.setting['cols']):
                self.add_widget(Field(text=f"{i} {j}" ,row_id=i ,col_id=j))

    def random_bom(self, rows: int, cols: int, bom_number: int) -> list:
        mine = [[0]*cols for i in range(rows)]

        counter = min(bom_number,rows*cols)

        while (counter > 0):
            row_random = random.randint(0,rows-1)
            col_random = random.randint(0,cols-1)

            if(mine[row_random][col_random] != -1):
                mine[row_random][col_random] = -1
                counter -= 1

        return mine

class SimpleApp(App):
    def build(self):
        self.setting = dict(rows=8,cols=8,bom_number=10,right_click_count=0)
        self.sm = ScreenManager()
        self.sm.add_widget(LoginScreen(name='login',setting = self.setting))
        self.sm.add_widget(PlayGameScreen(name='game',setting = self.setting))
        return self.sm

if __name__ == '__main__':
    SimpleApp().run()